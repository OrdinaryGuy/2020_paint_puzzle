/*
    Alojz �olt�s 2020 Paint
    Demonstration project in C language which creates painted puzzles.

    Everything you need to know is in help function when you press 'h' in main program.
    Be aware that program does not check if your result is solveable.
    Do not change data in .txt files. This may cause unexpected beavior.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

enum keys {KEY_UP = 72, KEY_DOWN = 80, KEY_RIGHT = 77, KEY_LEFT = 75,
         CTRL_UP = 141, CTRL_DOWN = 145, CTRL_RIGHT = 116, CTRL_LEFT = 115,
         ENTER = 13, ESCAPE = 27, BACK_SPACE = 8, SPACE = 32};

struct keyboard
{
    int inputChar;
    bool isArrow;
} iKey;

struct keyboard keyboardInput();
bool isAlphaNum();

struct workingBoard
{
    uint8_t board[30][30];
    uint8_t hNums[30][6];
    uint8_t vNums[30][6];
    uint8_t xCursor;
    uint8_t yCursor;
    uint8_t cursorColor;
}wBrd;

struct workingBoard initEmptyProject();

struct files
{
    char fileName[20];
    char filePath[35];
} f;

bool fileExists();
void saveProject();
void loadProject();

void mainMenu(bool* stop, bool* main);
void dispMainMenu();

void mainProgram(bool* main);
void displayMainProgram();

void generateData();
void displayGenData();

void displayHelp();

int main()
{
    system("color F0");
    system("cls");
    bool stop = false;
    bool main = false;
    printf("\n\n\tProject name: ");

    while(1)
    {
        keyboardInput();

        if(!main)
            mainMenu(&stop, &main);
        else
            mainProgram(&main);

        if(stop)
            return 0;

        if(!main)
            dispMainMenu();
        else
            displayMainProgram();
    }

    system("color 07");
    return 0;

}

struct keyboard keyboardInput()
{
    iKey.inputChar = _getch();

    if(iKey.inputChar == 224)
    {
        iKey.isArrow = true;
        iKey.inputChar = _getch();
    }
    else
        iKey.isArrow = false;

    return iKey;
}

struct workingBoard initEmptyProject()
{
    for(int i = 0; i < 900; i++)
        *(&(wBrd.board[0][0]) + i) = 0;
    for(int i = 0; i < 180; i++)
        *(&(wBrd.hNums[0][0]) + i) = 0;
    for(int i = 0; i < 180; i++)
        *(&(wBrd.vNums[0][0]) + i) = 0;
    wBrd.xCursor = 0;
    wBrd.yCursor = 0;
    return wBrd;
}

bool isAlphaNum()
{
    int iChar = iKey.inputChar;
    if(!iKey.isArrow)
    {
        if (/*iChar == 46 ||*/ iChar == 95) return true; // '.', '_'
        else if((iChar >= 48) && (iChar < 58)) return true; // 0-9
        else if((iChar >= 65) && (iChar < 91)) return true; // A-Z
        else if((iChar >= 97) && (iChar < 123)) return true; // a-z
        else return false;
    }
}

bool fileExists()
{
    strcpy(f.filePath, "Projects/");
    strcat(f.filePath, f.fileName);
    strcat(f.filePath, ".txt");
    if( access( f.filePath, F_OK ) != -1 )
        return true;
    else
        return false;
};

void mainMenu(bool* stop, bool* main)
{
    static int pos = 0;

//------------------- control part of main menu --------------------

    switch (iKey.inputChar)
    {
    case BACK_SPACE:    if(pos > 0)
                        {
                            pos--;
                            f.fileName[pos] = 0;
                        }
                        break;
    case ESCAPE:        *stop = true;
                        break;
    case ENTER:         if (strlen(f.fileName))
                        {
                            pos = 0;
                            *main = true;
                            if(fileExists())
                                loadProject();
                            else
                                wBrd = initEmptyProject();
                        }
                        break;
    default:            if(isAlphaNum())
                        {
                            if(pos < 19)
                            {
                                f.fileName[pos] = (char)(iKey.inputChar);
                                f.fileName[pos+1] = 0;
                                pos++;
                            }
                        }
                        break;
    }
}

void dispMainMenu()
{
    system("cls");
    printf("\n\n\tProject name: %s", f.fileName);

    // shows when no name is enterred
    if(iKey.inputChar == ENTER)
        printf("Enter a name");

    if(strlen(f.fileName))
    {
        if(fileExists())
            printf("\n\n\tExisting file\n\n");
        else
            printf("\n\n\tUnique name\n\n");
    }
}

void mainProgram(bool* main)
{
    if(iKey.isArrow)
    {
        switch (iKey.inputChar)
        {
            case KEY_RIGHT:     if(wBrd.xCursor < 29)
                                    (wBrd.xCursor)++;
                                else
                                    (wBrd.xCursor) = 0;
                                break;
            case KEY_LEFT:      if(wBrd.xCursor > 0)
                                    (wBrd.xCursor)--;
                                else
                                    (wBrd.xCursor) = 29;
                                break;
            case KEY_UP:        if(wBrd.yCursor > 0)
                                    (wBrd.yCursor)--;
                                else
                                    (wBrd.yCursor) = 29;
                                break;
            case KEY_DOWN:      if(wBrd.yCursor < 29)
                                    (wBrd.yCursor)++;
                                else
                                    (wBrd.yCursor) = 0;
                                break;
            case CTRL_RIGHT:    for(int y = 0; y < 30; y++)
                                {
                                    for(int x = 29; x > 0; x--)
                                        wBrd.board[x][y] = wBrd.board[x - 1][y];
                                    wBrd.board[0][y] = 0;
                                }
                                break;
            case CTRL_LEFT:     for(int y = 0; y < 30; y++)
                                {
                                    for(int x = 0; x < 29; x++)
                                        wBrd.board[x][y] = wBrd.board[x + 1][y];
                                    wBrd.board[29][y] = 0;
                                }
                                break;
            case CTRL_UP:       for(int x = 0; x < 30; x++)
                                {
                                    for(int y = 0; y < 29; y++)
                                        wBrd.board[x][y] = wBrd.board[x][y + 1];
                                    wBrd.board[x][29] = 0;
                                }
                                break;
            case CTRL_DOWN:     for(int x = 0; x < 30; x++)
                                {
                                    for(int y = 29; y > 0; y--)
                                        wBrd.board[x][y] = wBrd.board[x][y - 1];
                                    wBrd.board[x][0] = 0;
                                }
                                break;
        }
    }
    else
    {
        switch (iKey.inputChar)
        {
        case ESCAPE:        *main = false;
                            strcpy(f.fileName, "");
                            break;

        case SPACE:         if(wBrd.board[wBrd.xCursor][wBrd.yCursor] == 0)
                                wBrd.board[wBrd.xCursor][wBrd.yCursor] = 1;
                            else
                                wBrd.board[wBrd.xCursor][wBrd.yCursor] = 0;
                            break;
        case 'g':           displayGenData();
                            break;
        case 'm':           wBrd.cursorColor = wBrd.board[wBrd.xCursor][wBrd.yCursor];
                            if(wBrd.cursorColor == 1) wBrd.cursorColor = 0;
                            else wBrd.cursorColor = 1;
                            for (int x = 0; x < 30; x++)
                                wBrd.board[x][wBrd.yCursor] = wBrd.cursorColor;
                            break;
        case 'n':           wBrd.cursorColor = wBrd.board[wBrd.xCursor][wBrd.yCursor];
                            if(wBrd.cursorColor == 1) wBrd.cursorColor = 0;
                            else wBrd.cursorColor = 1;
                            for (int y = 0; y < 30; y++)
                                wBrd.board[wBrd.xCursor][y] = wBrd.cursorColor;
                            break;
        case 'h':           displayHelp();
                            break;
        case 's':           saveProject();
                            break;
        }
    }
}

void displayMainProgram()
{
    const char cursor[] = {254, 0};
    const char full[] = {219, 0};
    system("cls");
    printf("\n+------------------------------+\n");
    for(int y = 0; y < 30; y++)
    {
        printf("|");
        for(int x = 0; x < 30; x++)
        {
            if(y == wBrd.yCursor && x == wBrd.xCursor)
                printf("%c", 254);
            else if(wBrd.board[x][y] == 0)
                printf(" ");
            else if(wBrd.board[x][y] == 1)
                printf("%c", 219);
        }
        printf("|\n");
    }
    printf("+------------------------------+\n\n\nPress 'h' for help");
}

void generateData()
{
    //--------------------- Generate v and h values ------------------------
    //--------------------- vertical values --------------------------------
    int vVal;
    int previousVal;
    for(int x = 0; x < 30; x++)
    {
        for(int i = 0; i < 6; i++)
            wBrd.vNums[x][i] = 0;

        vVal = 0;
        int previousVal = 0;
        for(int y = 0; y < 30; y++)
        {
            if(wBrd.board[x][y] == 1 && previousVal == 0)
            {
                previousVal = 1;
                vVal++;
            }
            else if(wBrd.board[x][y] == 1 && previousVal == 1)
            {
                vVal++;
            }
            if((wBrd.board[x][y] == 0 && previousVal == 1) || ((y == 29 && wBrd.board[x][29] == 1)))
            {
                previousVal = 0;
                int i = 0;
                while((wBrd.vNums[x][i] != 0) && (i < 5))
                    i++;
                while(i >= 0)
                {
                    wBrd.vNums[x][i + 1] = wBrd.vNums[x][i];
                    i--;
                }
                wBrd.vNums[x][0] = vVal;
                vVal = 0;
            }
        }
    }
    //--------------------- horizontal values ------------------------------

    for(int y = 0; y < 30; y++)
    {
        for(int i = 0; i < 6; i++)
            wBrd.hNums[y][i] = 0;

        vVal = 0;
        int previousVal = 0;
        for(int x = 0; x < 30; x++)
        {
            if(wBrd.board[x][y] == 1 && previousVal == 0)
            {
                previousVal = 1;
                vVal++;
            }
            else if(wBrd.board[x][y] == 1 && previousVal == 1)
            {
                vVal++;
            }
            if((wBrd.board[x][y] == 0 && previousVal == 1) || (x == 29 && wBrd.board[29][y] == 1))
            {
                previousVal = 0;
                int i = 0;
                while((wBrd.hNums[y][i] != 0) && (i < 5))
                    i++;
                while(i >= 0)
                {
                    wBrd.hNums[y][i + 1] = wBrd.hNums[y][i];
                    i--;
                }
                wBrd.hNums[y][0] = vVal;
                vVal = 0;
            }
        }
    }
}

void displayGenData()
{
    generateData();

    system("cls");
    for(int j = 5; j >= 0; j--)
    {
        printf("                   ");
        for(int i = 0; i < 30; i++)
        {
            if(wBrd.vNums[i][j] < 10)
                printf(" ");
            printf("%d", wBrd.vNums[i][j]);
        }
        printf("\n");
    }
    printf("                  +------------------------------------------------------------\n");

    for(int i = 0; i < 30; i++)
    {
        for(int j = 5; j >= 0; j--)
        {
            if(wBrd.hNums[i][j] < 10)
                printf("  ");
            else
                printf(" ");
            printf("%d", wBrd.hNums[i][j]);
        }
        printf("|");
        for(int j = 0; j < 30; j++)
        {
                printf("%x ", wBrd.board[j][i]);
        }
        printf("\n");
    }

    printf("\n\nPress any key to continue");
    _getch();
}

void displayHelp()
{
    system("cls");
    printf("INSTRUCTIONS:\n\n");
    printf("ARROWS\t\tmoving cursor in desired direction\n");
    printf("SPACE\t\tchanging color of the spot where cursor points to\n");
    printf("SHIFT_ARROWS\tmoving whole picture in desired direction\n");
    printf("m\t\tchanging color of whole horizontal line\n");
    printf("n\t\tchanging color of whole vertical line\n");
    printf("g\t\tgenerate horizontal and vertical values of the picture\n");
    printf("s\t\tsave project\n");
    printf("ESCAPE\t\treturn back or quit program\n");
    printf("\nNote that picture puzzle does not have to be solveable\n");
    printf("\nPress any key to continue\n");
    _getch();
}

void saveProject()
{
    FILE* WorkFile;
    WorkFile = fopen(f.filePath, "w");
    for(int y = 0; y < 30; y++)
    {
        for(int x = 0; x < 30; x++)
            fprintf(WorkFile, "%x", wBrd.board[x][y]);
        fprintf(WorkFile, "\n");
    }

    generateData();
    for(int i = 5; i >= 0; i--)
    {
        fprintf(WorkFile, "                   ");
        for(int x = 0; x < 30; x++)
        {
            if(wBrd.vNums[x][i] == 0)
                fprintf(WorkFile, "   ", wBrd.vNums[x][i]);
            else if(wBrd.vNums[x][i] < 10)
                fprintf(WorkFile, "  %d", wBrd.vNums[x][i]);
            else
                fprintf(WorkFile, " %d", wBrd.vNums[x][i]);
        }
        fprintf(WorkFile, "\n");
    }
    fprintf(WorkFile, "                  +------------------------------------------------------------------------------------------\n");

    for(int y = 0; y < 30; y++)
    {
        for(int i = 5; i >= 0; i--)
        {
            if(wBrd.hNums[y][i] == 0)
                fprintf(WorkFile, "   ", wBrd.hNums[y][i]);
            else if(wBrd.hNums[y][i] < 10)
                fprintf(WorkFile, "  %d", wBrd.hNums[y][i]);
            else
                fprintf(WorkFile, " %d", wBrd.hNums[y][i]);
        }
        fprintf(WorkFile, "|\n");
    }

    fclose(WorkFile);

    printf("\n\nProject named %s has been saved.\nPress any key to continue", f.fileName);
    _getch();
}

void loadProject()
{
    FILE* WorkFile;
    WorkFile = fopen(f.filePath, "r");

    char buff[2];
    int number;
    int x = 0;
    int y = 0;
    do
    {
        do
        {
            fgets(buff, 2, (FILE*)WorkFile);
            if (buff[0] > 47 && buff[0] < 58) // 0-9
            {
                number = buff[0] - '0';
            }
            else if (buff[0] > 96 && buff[0] < 103)// a-f
            {
                number = buff[0] - 'a' + 10;
            }
            else
                continue;
            wBrd.board[x][y] = number;
            x++;
        }
        while(x < 30);
        y++;
        x = 0;
    }
    while(y < 30);

    fclose(WorkFile);
}
